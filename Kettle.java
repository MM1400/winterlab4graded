public class Kettle {
	private String colour;
	private int quantity;
	private String brand;
	
	public Kettle(String colour, int quantity, String brand) {
		this.colour = colour;
		this.quantity = quantity;
		this.brand = brand;
	}
	
	public String getColour() {
		return this.colour;
	}	
	public String getBrand() {
		return this.brand;
	}	
	public int getQuantity() {
		return this.quantity;
	}
	public void setColour(String colour) {
	this.colour = colour;
	}
	public void setBrand(String brand) {
	this.brand = brand;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void printBrand(String brand) {
		System.out.println(brand + " goes brrrrrrrrrrr");
	}
	public void printQuantity(int quantity) {
		System.out.println("The kettle can hold a maximum of " + quantity + " ml");
	}
	public void addQuantity(int quantity) {
		int validQuantity = positiveQuantity(quantity);
		this.quantity = this.quantity + validQuantity;
	}
	private int positiveQuantity(int quantity) {
		java.util.Scanner scan = new java.util.Scanner(System.in);
		while(quantity <= 0) {
		System.out.println("Enter a value above 0");
		quantity = scan.nextInt();
		}
		return quantity;
	}
}
