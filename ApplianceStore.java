import java.util.Scanner;
public class ApplianceStore {
		public static void main(String[] args) {
			Scanner scan = new Scanner(System.in);
			Kettle[] kettles = new Kettle[4];
			for(int i=0; i < kettles.length; i++) {
				System.out.println("Enter the colour");
				String colour = scan.next();
				System.out.println("Enter the quantity");
				int quantity = scan.nextInt();
				System.out.println("Enter the brand");
				String brand = scan.next();
				kettles[i] = new Kettle(colour,quantity,brand);
			}
			System.out.println("Enter colour for kettle[3]");
			kettles[3].setColour(scan.next());
			System.out.println("Enter quantity for kettle[3]");
			kettles[3].setQuantity(scan.nextInt());
			System.out.println("Enter brand for kettle[3]");
			kettles[3].setBrand(scan.next());
			kettles[1].addQuantity(-500);
			System.out.println("The result of addQuantity is " + kettles[1].getQuantity());
			System.out.println("Brand of kettle[3] is " +kettles[3].getBrand() + ". Colour is " 
			+ kettles[3].getColour() + ". Quantity is " + kettles[3].getQuantity());
			kettles[0].printBrand(kettles[0].getBrand());
			kettles[0].printQuantity(kettles[0].getQuantity());
			
		}
}
